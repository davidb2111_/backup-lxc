#!/bin/bash

# EXCLUDE: space separated list of lxc container to exclude
# TMPDIR: temp dir for storing backup (some backup destination dir does not accept direct lxc export, like curlftpfs)
# BKPDIR: directory to put backup in
# PRECOMMAND: command to execute before starting backups
# POSTCOMMAND: command to execute after all backup completes
EXCLUDE=
TMPDIR=/dev/shm/backup-lxc
BKPDIR=backup
PRECOMMAND=""
POSTCOMMAND=""


[ ! -d "$TMPDIR" ] && mkdir -p "$TMPDIR"

[ -f ~/.config/backup-container ] && . ~/.config/backup-container

[ -n "$PRECOMMAND" ] && bash -l -c "$PRECOMMAND"
if [ $? -ne 0 ]; then
  echo "Precommand failed aborting"
  exit 1
fi

date=$(date +%Y%m%d)

lxc ls -cn --format csv |while read container; do
  if echo "$EXCLUDE" |grep -q -e "\b$container\b"; then
    echo "skipping $container"
    continue
  fi
  echo "start backup of $container"
  backupname=$container-$date.tar.gz
  if ! lxc export "$container" "$TMPDIR/$backupname"; then
    echo "error on export"
    continue
  fi 
  if mv "$TMPDIR/$backupname" "$BKPDIR/$backupname"; then
    echo "backup successfull"
    echo "removing old backup"
    find "$BKPDIR" -name "$container-*" ! -name "$backupname" -delete
    echo "done"
  else
    echo "error on backup copy to $BKPDIR"
  fi
done

rm -rf "$TMPDIR"

[ -n "$POSTCOMMAND" ] && bash -l -c "$POSTCOMMAND"
